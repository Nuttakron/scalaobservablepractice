package src.com.ndheeravorav.observable.practice

import java.util.concurrent.Executors

import monix.eval.Task
import monix.execution.ExecutionModel.AlwaysAsyncExecution
import monix.execution.Scheduler
import monix.reactive.Observable
import src.com.ndheeravorav.observable.Service

import scala.concurrent.{Await, ExecutionContext, Future}

import scala.concurrent.duration._

class ObservableWithConcurrent {

  val ecThreadPool = 10
  val schedulerPool = 5

  implicit val ec : ExecutionContext = new ExecutionContext {
    val threadPool = Executors.newFixedThreadPool(ecThreadPool);

    def execute(runnable: Runnable) {
      threadPool.submit(runnable)
    }

    def reportFailure(t: Throwable) {}
  }

  implicit val scheduler: Scheduler = Scheduler.computation(
    parallelism = schedulerPool,
    executionModel = AlwaysAsyncExecution
  )

  //ecThreadPool
  //schedulerPool
  val parallelismPerTimes = 20

  val r = Observable.range(0, 100000)
    .mapParallelUnordered(parallelismPerTimes)(x => {
      println(s"start - $x")

      //Should not return as task.now because, will return as task then do next job immediately, even current task not complete
      //For example ec was 10 but set parallelism as 1
      //Observable will do 10 times per times but expect should excuse as 1 until finish then pick next
      //val id = Task.now( Service.call(x.toInt, 20000)(ec) )

      //Task from future is correct
      val id = Task.fromFuture( Service.call(x.toInt, 20000)(ec) )

      //Thread.sleep(20000)
      //val id = Task.now(x.toInt)

      //val id = Task.now( Await.result( Service.call(x.toInt, 20000)(ec), 20000 seconds ) )

      println(s"stop - $x")

      id
    })
    .runAsyncGetLast(scheduler).flatMap( x => Future.successful(x) )



  val result = Await.result(r, 100 seconds)

  println(s"*************** End result with $result *******************")

}
