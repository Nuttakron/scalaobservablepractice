package src.com.ndheeravorav.observable.practice

import java.util.concurrent.Executors

import monix.eval.Task
import monix.execution.ExecutionModel.AlwaysAsyncExecution
import monix.execution.Scheduler
import monix.reactive.Observable
import src.com.ndheeravorav.observable.Service
import scala.concurrent.duration._

import scala.concurrent.{Await, ExecutionContext, Future}

//https://monix.io/docs/3x/tutorials/parallelism.html

class ObservableWithTask {

  val ecThreadPool = 10
  val schedulerPool = 5

  implicit val ec : ExecutionContext = new ExecutionContext {
    val threadPool = Executors.newFixedThreadPool(ecThreadPool);

    def execute(runnable: Runnable) {
      threadPool.submit(runnable)
    }

    def reportFailure(t: Throwable) {}
  }

  implicit val scheduler: Scheduler = Scheduler.computation(
    parallelism = schedulerPool,
    executionModel = AlwaysAsyncExecution
  )

  //ecThreadPool
  //schedulerPool
  val parallelismPerTimes = 20

  val listOfFuture = Iterator(
    Task.fromFuture( Service.call(0, 2000)(ec) ),
    Task.fromFuture( Service.call(1, 6000)(ec) ),
    Task.fromFuture( Service.call(2, 4000)(ec) )
  )

  val timeToEmit = 5 seconds



  val task = Observable.fromIterator(listOfFuture, ()=>{} )
    .mapParallelUnordered(parallelismPerTimes)(x => x)
//        .bufferTimed(timeToEmit)
//        .map(x => {
//          println("emit" + x )
//          x
//        })
    .toListL

  val r = Observable.fromTask( task )
    .runAsyncGetLast(scheduler)
    .flatMap( x => Future.successful(x) )

  val result = Await.result(r, 10 seconds)

  println(s"*************** End result with $result *******************")

}
