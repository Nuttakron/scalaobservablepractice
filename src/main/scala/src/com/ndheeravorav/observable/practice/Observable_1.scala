package src.com.ndheeravorav.observable.practice

import monix.eval.Task
import monix.execution.ExecutionModel.AlwaysAsyncExecution
import monix.execution.Scheduler
import monix.reactive.Observable

import scala.concurrent.Await
import scala.concurrent.duration._

class Observable_1 {

  val schedulerPool = 5

  implicit val scheduler: Scheduler = Scheduler.computation(
    parallelism = schedulerPool,
    executionModel = AlwaysAsyncExecution
  )

  val observable = Observable.fromTask(Task.now("Hello World"))
  val promise = observable.runAsyncGetLast
  val result = Await.result(promise, 1 seconds)

  println(result)

}
