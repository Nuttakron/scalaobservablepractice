package src.com.ndheeravorav.observable

import java.time.LocalDateTime

import scala.concurrent.{ExecutionContext, Future}

object Service {

  def call( id: Int, sleepTime: Long = 1000 )(implicit ec: ExecutionContext) = {

    Future {

      val startTime = LocalDateTime.now()

      println(s"[Pre] Process id: $id -- Start $startTime")
      Thread.sleep(sleepTime)

      val endTime = LocalDateTime.now()

      println(s"[After] Process id: $id -- Start $startTime End $endTime ")
      id
    }

  }



}
